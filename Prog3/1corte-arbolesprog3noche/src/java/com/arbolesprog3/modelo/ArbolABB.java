/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modelo;

import com.arbolesprog3.excepcion.CelularExcepcion;
import com.arbolesprog3.validador.CelularValidador;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author carloaiza
 */
public class ArbolABB implements Serializable {

    private NodoABB raiz;
    private int cantidadNodos;

    public ArbolABB() {
    }

    public NodoABB getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoABB raiz) {
        this.raiz = raiz;
    }

    public int getCantidadNodos() {
        return cantidadNodos;
    }

    public void setCantidadNodos(int cantidadNodos) {
        this.cantidadNodos = cantidadNodos;
    }

    ///Adicionar en el árbol
    public void adicionarNodo(Celular dato) throws CelularExcepcion {

        CelularValidador.validarDatos(dato);
        NodoABB nuevo = new NodoABB(dato);
        if (raiz == null) {
            raiz = nuevo;
        } else {
            adicionarNodo(nuevo,raiz);
        }
        cantidadNodos++;
    }

    private void adicionarNodo(NodoABB nuevo, NodoABB pivote)
            throws CelularExcepcion {
        if (nuevo.getDato().getImei().compareTo(pivote.getDato().getImei()) == 0) {
            throw new CelularExcepcion("Ya existe un celular con el imei "
                    + nuevo.getDato().getImei());
        } else if (nuevo.getDato().getImei().compareTo(pivote.getDato().
                getImei()) < 0) {
            //Va por la izq
            if (pivote.getIzquierda() == null) {
                pivote.setIzquierda(nuevo);
            } else {
                adicionarNodo(nuevo, pivote.getIzquierda());
            }
        } else {
            //Va por la derecha
            if (pivote.getDerecha()== null) {
                pivote.setDerecha(nuevo);
            } else {
                adicionarNodo(nuevo, pivote.getDerecha());
            }
        }
    }
    
    
     public List<Celular> recorrerInOrden() {
        List<Celular> listaCelulares=new ArrayList<>();
        recorrerInOrden(raiz,listaCelulares);
        return listaCelulares;
    }

    private void recorrerInOrden(NodoABB reco,List<Celular> listado) {
        if (reco != null) {
            recorrerInOrden(reco.getIzquierda(),listado);
            listado.add(reco.getDato());
            recorrerInOrden(reco.getDerecha(),listado);
        }
    }
    
    
     public List<Celular> recorrerPreOrden() {
        List<Celular> listaCelulares=new ArrayList<>();
        recorrerPreOrden(raiz,listaCelulares);
        return listaCelulares;
    }

    private void recorrerPreOrden(NodoABB reco,List<Celular> listado) {
        if (reco != null) {
            listado.add(reco.getDato());
            recorrerPreOrden(reco.getIzquierda(),listado);            
            recorrerPreOrden(reco.getDerecha(),listado);
        }
    }
    
     public List<Celular> recorrerPostOrden() {
        List<Celular> listaCelulares=new ArrayList<>();
        recorrerPostOrden(raiz,listaCelulares);
        return listaCelulares;
    }

    private void recorrerPostOrden(NodoABB reco,List<Celular> listado) {
        if (reco != null) {
            
            recorrerPostOrden(reco.getIzquierda(),listado);            
            recorrerPostOrden(reco.getDerecha(),listado);
            listado.add(reco.getDato());
        }
    }
    
    public boolean esVacio()
    {
        return raiz==null;
    }
}
