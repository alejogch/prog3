/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.controlador;

import com.arbolesprog3.excepcion.CelularExcepcion;
import com.arbolesprog3.modelo.ArbolABB;
import com.arbolesprog3.modelo.Celular;
import com.arbolesprog3.modelo.Marca;
import com.arbolesprog3.modelo.Operador;
import com.arbolesprog3.utilidades.JsfUtil;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.DiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.StraightConnector;
import org.primefaces.model.diagram.endpoint.DotEndPoint;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;

/**
 *
 * @author carloaiza
 */
@Named(value = "controladorABB")
@SessionScoped
public class ControladorABB implements Serializable {

    private boolean verRegistrar = false;
    private DefaultDiagramModel model;
    private List<Marca> marcas;
    private List<Operador> operadores;
    private Celular celular= new Celular();
    
    private ArbolABB arbol= new ArbolABB();

    /**
     * Creates a new instance of ControladorABB
     */
    public ControladorABB() {
    }

    public ArbolABB getArbol() {
        return arbol;
    }

    public void setArbol(ArbolABB arbol) {
        this.arbol = arbol;
    }
    
    

    public Celular getCelular() {
        return celular;
    }

    public void setCelular(Celular celular) {
        this.celular = celular;
    }

    
    
    
    public List<Marca> getMarcas() {
        return marcas;
    }

    public void setMarcas(List<Marca> marcas) {
        this.marcas = marcas;
    }

    public List<Operador> getOperadores() {
        return operadores;
    }

    public void setOperadores(List<Operador> operadores) {
        this.operadores = operadores;
    }

    
    
    @PostConstruct
    private void iniciar() {
        llenarMarcas();
        llenarOperadores();
        pintarArbol();
        
    }

    public boolean isVerRegistrar() {
        return verRegistrar;
    }

    public void setVerRegistrar(boolean verRegistrar) {
        this.verRegistrar = verRegistrar;
    }

    public void habilitarRegistrar() {
        verRegistrar = true;
        celular = new Celular();
    }

    public void deshabilitarRegistrar() {
        verRegistrar = false;
    }

    public void pintarArbol() {
        model = new DefaultDiagramModel();
        model.setMaxConnections(-1);

        Element ceo = new Element("CEO", "25em", "6em");
        ceo.addEndPoint(createEndPoint(EndPointAnchor.BOTTOM));
        model.addElement(ceo);

        //CFO
        Element cfo = new Element("CFO", "10em", "18em");
        cfo.addEndPoint(createEndPoint(EndPointAnchor.TOP));
        cfo.addEndPoint(createEndPoint(EndPointAnchor.BOTTOM));
        cfo.addEndPoint(createEndPoint(EndPointAnchor.LEFT));

        Element fin = new Element("FIN", "5em", "30em");
        fin.addEndPoint(createEndPoint(EndPointAnchor.TOP));

        Element pur = new Element("PUR", "20em", "30em");
        pur.addEndPoint(createEndPoint(EndPointAnchor.TOP));

        model.addElement(cfo);
        model.addElement(fin);
        model.addElement(pur);

        //CTO
        Element cto = new Element("CTO", "40em", "18em");
        cto.addEndPoint(createEndPoint(EndPointAnchor.TOP));
        cto.addEndPoint(createEndPoint(EndPointAnchor.BOTTOM));

        Element dev = new Element("DEV", "35em", "30em");
        dev.addEndPoint(createEndPoint(EndPointAnchor.TOP));

        Element tst = new Element("TST", "50em", "30em");
        tst.addEndPoint(createEndPoint(EndPointAnchor.TOP));

        model.addElement(cto);
        model.addElement(dev);
        model.addElement(tst);

        StraightConnector connector = new StraightConnector();
        connector.setPaintStyle("{strokeStyle:'#CC3a4e', lineWidth:5}");
        connector.setHoverPaintStyle("{strokeStyle:'#20282b'}");

        //connections
        model.connect(new Connection(ceo.getEndPoints().get(0), cfo.getEndPoints().get(0), connector));
        model.connect(new Connection(ceo.getEndPoints().get(0), cto.getEndPoints().get(0), connector));
        model.connect(new Connection(cfo.getEndPoints().get(1), fin.getEndPoints().get(0), connector));
        model.connect(new Connection(cfo.getEndPoints().get(1), pur.getEndPoints().get(0), connector));
        model.connect(new Connection(cto.getEndPoints().get(1), dev.getEndPoints().get(0), connector));
        model.connect(new Connection(cto.getEndPoints().get(1), tst.getEndPoints().get(0), connector));
    }

    private EndPoint createEndPoint(EndPointAnchor anchor) {
        DotEndPoint endPoint = new DotEndPoint(anchor);
        endPoint.setStyle("{fillStyle:'#404a4e'}");
        endPoint.setHoverStyle("{fillStyle:'#20282b'}");

        return endPoint;
    }

    public DiagramModel getModel() {
        return model;
    }
    
    private void llenarMarcas()
    {
        marcas= new ArrayList<>();
        marcas.add(new Marca((short)1, "Huawei"));
        marcas.add(new Marca((short)2, "LG"));
        marcas.add(new Marca((short)3, "Sony"));
        
    }

    private void llenarOperadores()
    {
        operadores= new ArrayList<>();
        operadores.add(new Operador("Movistar", "Manizales", "4G"));
        operadores.add(new Operador("Claro", "Pereira", "3G"));
        
    }
    
    public void guardarCelular()
    {
        try {
            arbol.adicionarNodo(celular);
            celular = new Celular();
            verRegistrar=false;
            pintarArbol();
        } catch (CelularExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }
    
}
